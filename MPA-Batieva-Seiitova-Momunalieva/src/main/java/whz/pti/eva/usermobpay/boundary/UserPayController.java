package whz.pti.eva.usermobpay.boundary;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import whz.pti.eva.usermobpay.domain.PayUser;
import whz.pti.eva.usermobpay.domain.State;
import whz.pti.eva.usermobpay.service.PayUserException;
import whz.pti.eva.usermobpay.service.PayUserService;
import whz.pti.eva.usermobpay.service.dto.AccountResponseDTO;
import whz.pti.eva.usermobpay.service.dto.PayUserResponseDTO;
import whz.pti.eva.usermobpay.service.dto.TransferDTO;


@RestController
@RequestMapping(value = "/users")
public class UserPayController {


    @Autowired
    private PayUserService payUserService;


    @RequestMapping(value = "/{userId}") //, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listPayUser(@PathVariable String userId) {
        PayUser payUser = payUserService.getPayUser(userId);
        PayUserResponseDTO payUserResponseDTO = new PayUserResponseDTO(payUser);
        return ResponseEntity.status(HttpStatus.OK).body(payUserResponseDTO);
    }

    @RequestMapping(value = "/{userId}/account") //, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> listAccountBalance(@PathVariable String userId) {
        State payUserState = payUserService.getState(userId);
        if (payUserState == State.available) {//        return new ResponseEntity<Authenticator.Success>(HttpStatus.OK);
            double balance = payUserService.getAccountBalanceByName(userId);
            return ResponseEntity.status(HttpStatus.OK).body(new AccountResponseDTO("Kontostand betraegt " + balance));
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AccountResponseDTO("transferNotAllowed"));
        }
    }

    @RequestMapping(value = "/{userId}/opened", method = RequestMethod.PUT)
    public ResponseEntity<?> openAccount(@PathVariable String userId) {
        payUserService.openAccount(userId);
        return ResponseEntity.status(HttpStatus.OK).body(new AccountResponseDTO("Konto steht nun zur Verfuegung"));
    }


    @RequestMapping(value = "/{userId}/payment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> transfer(@PathVariable String userId, @RequestBody TransferDTO input) {
        String returnStatus = null;
        try {
            returnStatus = payUserService.transfer(userId, input.getAmount());
        } catch (PayUserException e) {
            e.printStackTrace();
        }

        if (returnStatus.equals("okay"))
            return ResponseEntity.status(HttpStatus.CREATED).body(new AccountResponseDTO("Transfer ist erfolgreich durchgefuehrt"));
        else if (returnStatus.equals("nichtGenugGeld"))
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new AccountResponseDTO("Nicht genug Geld beim Kunde"));
        else
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AccountResponseDTO(returnStatus));
    }

    @RequestMapping(value = "/{userId}/deleted", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUser(@PathVariable String userId) {
        payUserService.deleteUser(userId);
        return ResponseEntity.status(HttpStatus.OK).body(new AccountResponseDTO("Nutzer und Konto ist nun geloescht"));
    }

    @RequestMapping(value = "/{userId}/suspended", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> changeUserToSuspendedState(@PathVariable String userId, @RequestBody String state) {
        if (State.valueOf(state) == State.suspended) {
            payUserService.changeUserToSuspendedState(userId);
            return ResponseEntity.status(HttpStatus.OK).body(new AccountResponseDTO("suspended"));
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new AccountResponseDTO("Falscher Zustand eingegeben"));
    }


    

}
