package whz.pti.eva.usermobpay.service;

import whz.pti.eva.usermobpay.domain.PayUser;
import whz.pti.eva.usermobpay.domain.State;

public interface PayUserService {
    double getAccountBalanceByName(String userId);

    boolean containsAndAvailable(String userId);

    State getState(String userId);

    void openAccount(String userId);

    String transfer(String from, double amount) throws PayUserException;

    boolean deleteUser(String userId);

    void changeUserToSuspendedState(String userId);

    PayUser getPayUser(String userId);
}

