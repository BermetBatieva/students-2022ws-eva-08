package whz.pti.eva.usermobpay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import whz.pti.eva.usermobpay.domain.PayUser;
import whz.pti.eva.usermobpay.domain.PayUserRepository;
import whz.pti.eva.usermobpay.domain.State;
import javax.transaction.Transactional;


import java.util.Optional;

@Service
public class PayUserServiceImpl implements PayUserService{

    @Autowired
    private PayUserRepository payUserRepository;

    @Override
    public double getAccountBalanceByName(String userId) {
        PayUser payUser;
        try {
            payUser = payUserRepository.findByName(userId)
                    .orElseThrow(() -> new PayUserException("user cannot be found"));
        } catch (PayUserException e) {
            e.printStackTrace();
            return -101010;
        }
        return payUser.getAccount().getBalance();
    }

    @Override
    public boolean containsAndAvailable(String userId) {
        return payUserRepository.existsByNameAndAvailable(userId);

    }

    @Override
    public State getState(String userId) {
        Optional<PayUser> userPayOptional = payUserRepository.findByName(userId);
        if (userPayOptional.isPresent()) {
            // value is present inside Optional
            return userPayOptional.get().getState();

        } else {
            // value is absent
            return State.doesNotExist;
        }
    }


    @Override
    public void openAccount(String userId) {
        PayUser payUser = payUserRepository.findByName(userId).orElse(new PayUser(userId));
        payUser.setState(State.available);
        payUserRepository.save(payUser);

    }

    @Override
    @Transactional
    public String transfer(String from, double amount) throws PayUserException {
        if (!isPaymentPossibleCheck(from, amount))
            return "nichtGenugGeld";
        if (containsAndAvailable(from) && containsAndAvailable("pizzaAccount")) {
            PayUser payUserFrom = payUserRepository
                    .findByName(from).orElseThrow(() -> new PayUserException("user cannot be found"));
            payUserFrom.getAccount().withdrawBalance(amount);

            PayUser payUserTo = payUserRepository
                    .findByName("pizzaAccount").orElseThrow(() -> new PayUserException("user cannot be found"));
            payUserTo.getAccount().depositBalance(amount);
            payUserRepository.save(payUserFrom);
            payUserRepository.save(payUserTo);
            return "okay";
        }
        else return "transferNotAllowed";

    }

    public boolean isPaymentPossibleCheck(String userId, double amount){
        Optional<PayUser> user = payUserRepository.findByName(userId);
        if(user.isEmpty())
            return false;
        double usersBalance = user.get().getAccount().getBalance();
        return usersBalance - amount >= 0;
    }

    @Override
    public boolean deleteUser(String userId) {
        Optional<PayUser> userPayOptional = payUserRepository.findByName(userId);
        if (userPayOptional.isPresent()) {
            PayUser payUser = userPayOptional.get();
            payUser.setState(State.doesNotExist);
            payUser.getAccount().setBalance(0);
            payUserRepository.save(payUser);
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void changeUserToSuspendedState(String userId) {
        payUserRepository.findByName(userId).ifPresent((userPay) -> {
            userPay.setState(State.suspended);
            payUserRepository.save(userPay);
        });
    }


    @Override
    public PayUser getPayUser(String userId) {
        Optional<PayUser> userPayOptional = payUserRepository.findByName(userId);
        if (userPayOptional.isPresent()) {
            return userPayOptional.get();
        };
        return null;
    }
}

