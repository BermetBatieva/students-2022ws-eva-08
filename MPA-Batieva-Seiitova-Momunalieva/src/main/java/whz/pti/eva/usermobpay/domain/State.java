package whz.pti.eva.usermobpay.domain;

public enum State {
    available, suspended, doesNotExist
}

