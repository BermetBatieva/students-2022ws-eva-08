package whz.pti.eva.usermobpay.config;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import whz.pti.eva.usermobpay.domain.PayUser;
import whz.pti.eva.usermobpay.domain.PayUserRepository;
import whz.pti.eva.usermobpay.domain.State;

@Component
public class InitializeDB {

    @Autowired
    private PayUserRepository payUserRepository;

    @PostConstruct
    public void init()  {
        PayUser payUser = new PayUser();
        payUser.setName("pizzaAccount");
        payUser.setState(State.available);
        payUserRepository.save(payUser);



    }
}

