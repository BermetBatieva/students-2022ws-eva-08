package whz.pti.eva.usermobpay.service;


public class PayUserException extends Exception{

    public PayUserException(String exception)
    {
        super(exception);
    }

}

