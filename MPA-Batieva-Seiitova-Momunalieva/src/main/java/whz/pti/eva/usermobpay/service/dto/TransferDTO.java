package whz.pti.eva.usermobpay.service.dto;


public class TransferDTO {

	String to = "pizzaAccount";
    double amount;

    public TransferDTO() {
    }

    public TransferDTO( double amount) {
        this.to = "pizzaAccount";
        this.amount = amount;
    }

    public String getTo() {
        return to;
    }

    public double getAmount() {
        return amount;
    }
}
