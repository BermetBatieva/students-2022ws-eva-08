package whz.pti.eva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MpaBatievaSeiitovaMomunalievaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpaBatievaSeiitovaMomunalievaApplication.class, args);
    }

}
