package whz.pti.eva.pizza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import whz.pti.eva.pizza.domain.Item;
import whz.pti.eva.pizza.domain.Ordered;
import whz.pti.eva.pizza.domain.OrderedItem;
import whz.pti.eva.pizza.domain.OrderedRepository;
import whz.pti.eva.pizza.dto.OrderDto;
import whz.pti.eva.security.domain.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class OrderedServiceImpl {


    @Autowired
    private OrderedRepository orderedRepository;

    @Autowired
    private UserRepository userRepository;


    public List<Ordered> getAllOrdersByCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userId = userRepository.getUserByLoginName(auth.getName()).getId().toString();
        if(orderedRepository.existsByUserId(userId)){
            return orderedRepository.findByUserId(userId);
        } else {
            return new ArrayList<>();
        }
    }


    public OrderDto getLastOrderByCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null) {
            List<Ordered> orders = orderedRepository.findByUserId(userRepository.getUserByLoginName(auth.getName()).getId().toString());
            List<OrderedItem> list = orders.get(orders.size() - 1).getItems();
            OrderDto orderDto = new OrderDto();
            orderDto.setOrderedItems(list);
            orderDto.setPrice(orders.get(orders.size() - 1).getTotalPrice());
            return orderDto;
        }
        return null;
    }



    public List<OrderDto> getAllOrdersOfCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        List<OrderDto> history = new ArrayList<>();
        if(auth != null) {
            List<Ordered> orders = orderedRepository.findByUserId(userRepository.getUserByLoginName(auth.getName()).getId().toString());
            int count = 0;
            for(Ordered order : orders){
                if(count == orders.size() - 1) break;
                OrderDto orderDto = new OrderDto();
                orderDto.setOrderedItems(order.getItems());
                orderDto.setPrice(order.getTotalPrice());
                history.add(orderDto);
                count++;
            }
            Collections.reverse(history);
            return history;
        }
        return new ArrayList<>();
    }
}
