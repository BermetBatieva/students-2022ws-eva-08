package whz.pti.eva.pizza.dto;

import whz.pti.eva.pizza.domain.OrderedItem;

import java.util.List;

public class OrderDto {

    private float price;
    private List<OrderedItem> orderedItems;


    public OrderDto() {
    }

    public OrderDto(float price, List<OrderedItem> orderedItems) {
        this.price = price;
        this.orderedItems = orderedItems;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public List<OrderedItem> getOrderedItems() {
        return orderedItems;
    }

    public void setOrderedItems(List<OrderedItem> orderedItems) {
        this.orderedItems = orderedItems;
    }
}
