package whz.pti.eva.pizza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;
import whz.pti.eva.pizza.domain.*;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserRepository;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ItemServiceImpl {

    private ItemRepository itemRepository;


    private PizzaRepository pizzaRepository;


    private CartRepository cartRepository;


    private UserRepository userRepository;


    private List<Item> temporaryCart;

    @Autowired
    public ItemServiceImpl(ItemRepository itemRepository,
                           PizzaRepository pizzaRepository,
                           CartRepository cartRepository, UserRepository userRepository) {
        this.itemRepository = itemRepository;
        this.pizzaRepository = pizzaRepository;
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;

        temporaryCart = new ArrayList<>();
    }




    //for temporary cart
    public List<Item> addToTemporaryCart(Long pizzaId,int quantity,PizzaSize pizzaSize) {
        Item item = new Item();
        item.setPizza(pizzaRepository.getById(pizzaId));
        item.setPizzaSize(pizzaSize);
        item.setQuantity(quantity);

        if(temporaryCart.size() == 0){
            if (pizzaSize.equals(PizzaSize.Large))
                item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                        .getPriceLarge().floatValue() * quantity));
            else if (pizzaSize.equals(PizzaSize.Small))
                item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                        .getPriceSmall().floatValue() * quantity));
            else if (pizzaSize.equals(PizzaSize.Medium))
                item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                        .getPriceMedium().floatValue() * quantity));
            temporaryCart.add(item);
        }else {
            boolean flag = false;
            for (Item item1 : temporaryCart) {
                if(item1.getPizza().getName().equals(item.getPizza().getName()) &&
                        item1.getPizzaSize().toString().equals(item.getPizzaSize().toString())){
                    flag = true;
                    item1.setQuantity(item1.getQuantity() + item.getQuantity());
                    if (pizzaSize.equals(PizzaSize.Large))
                        item1.setItemPrice(new BigDecimal(item1.getQuantity()
                                *pizzaRepository.getById(pizzaId).getPriceLarge().floatValue()));
                    else if (pizzaSize.equals(PizzaSize.Small))
                        item1.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                                .getPriceSmall().floatValue() * item1.getQuantity()));
                    else if (pizzaSize.equals(PizzaSize.Medium))
                        item1.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                                .getPriceMedium().floatValue() * item1.getQuantity()));
                    break;
                }
            }
            if(!flag){
                if (pizzaSize.equals(PizzaSize.Large))
                    item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                            .getPriceLarge().floatValue() * quantity));
                else if (pizzaSize.equals(PizzaSize.Small))
                    item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                            .getPriceSmall().floatValue() * quantity));
                else if (pizzaSize.equals(PizzaSize.Medium))
                    item.setItemPrice(new BigDecimal(pizzaRepository.getById(pizzaId)
                            .getPriceMedium().floatValue() * quantity));
                temporaryCart.add(item);
            }
        }

        return temporaryCart;
    }

    @Transactional
    public Cart addItemToCartAuth() {
        if(temporaryCart.size() == 0){
            return new Cart();
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            Cart cartForCustomer = new Cart();
            Optional<User> user = userRepository.findByLoginName(auth.getName());
            cartForCustomer.setCustomer(user.get());
            cartForCustomer.setStatus(Status.ACTIVE);
            cartRepository.save(cartForCustomer);
            for (Item item1 : temporaryCart) {
                itemRepository.save(item1);
                item1.setItemId(item1.getId() + item1.getPizzaSize().toString() + cartForCustomer.getId());
                itemRepository.save(item1);
                cartForCustomer.getItems().put(item1.getItemId(), item1);
            }
            cartRepository.save(cartForCustomer);
            cartForCustomer.setQuantity(cartForCustomer.getItems().size());
            cartRepository.save(cartForCustomer);
            temporaryCart.clear();
        }
        assert auth != null;
        return cartRepository.findByCustomer_LoginNameAndStatus(auth.getName(),Status.ACTIVE);
    }


    public List<Item> findAllItemsByCart() {
        return temporaryCart;
    }

    public int countItemsInCart(){
        List<Item> allItems = findAllItemsByCart();
        int count = 0;
        for(Item item : allItems){
            count += item.getQuantity();
        }
        return count;
    }

    public List<Item> deleteItem(int index){
        List<Item> items = findAllItemsByCart();
        items.remove(index);
        return temporaryCart;
    }

    public float countTotalPrise(){
        List<Item> allItemsInCart = findAllItemsByCart();
        if(allItemsInCart.size() < 1) return 0;
        float totalPrice = 0;
        for(Item item : allItemsInCart){
            totalPrice += item.getItemPrice().floatValue();
        }
        return totalPrice;
    }





}











