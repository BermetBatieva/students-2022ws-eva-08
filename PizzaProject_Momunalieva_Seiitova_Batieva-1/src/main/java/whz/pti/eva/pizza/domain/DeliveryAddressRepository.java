package whz.pti.eva.pizza.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress,Long> {

    List<DeliveryAddress> findDeliveryAddressByCustomerId(Long customerId);
}
