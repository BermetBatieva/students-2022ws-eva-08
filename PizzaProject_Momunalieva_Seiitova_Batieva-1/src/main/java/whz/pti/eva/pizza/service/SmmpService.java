package whz.pti.eva.pizza.service;

import whz.pti.eva.pizza.service.dto.PayActionResponseDTO;

public interface SmmpService {

    PayActionResponseDTO doPayAction(String from,String pcontent);
}

