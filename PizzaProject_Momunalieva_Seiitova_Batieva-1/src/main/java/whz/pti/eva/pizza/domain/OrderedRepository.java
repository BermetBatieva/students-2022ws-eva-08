package whz.pti.eva.pizza.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderedRepository extends JpaRepository<Ordered,Long> {


    List<Ordered> findByUserId(String userId);

    boolean existsByUserId(String userId);

}