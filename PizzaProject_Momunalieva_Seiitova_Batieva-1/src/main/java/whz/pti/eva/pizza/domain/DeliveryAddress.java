package whz.pti.eva.pizza.domain;

import whz.pti.eva.common.BaseEntity;
import whz.pti.eva.security.domain.User;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "delivery_address")
public class DeliveryAddress extends BaseEntity<Long> {

    private String street;

    private String houseNumber;

    private String town;

    private String postalCode;

    @ManyToMany
    private List<User> customer;

    public DeliveryAddress(String street, String houseNumber, String town, String postalCode, List<User> customer) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.town = town;
        this.postalCode = postalCode;
        this.customer = customer;
    }

    public DeliveryAddress() {

    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<User> getCustomer() {
        return customer;
    }

    public void setCustomer(List<User> customer) {
        this.customer = customer;
    }
}

