package whz.pti.eva.pizza.domain;

import whz.pti.eva.common.BaseEntity;
import whz.pti.eva.security.domain.User;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "cart")
public class Cart extends BaseEntity<Long> {

    private int quantity;

    @OneToMany
    private Map<String,Item> items;

    @OneToOne
    private User customer;

    @Enumerated(EnumType.STRING)
    private  Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Cart(int quantity, Map<String, Item> items, User customer, Status status) {
        this.quantity = quantity;
        this.items = items;
        this.customer = customer;
        this.status = status;
    }

    public Map<String, Item> getItems() {
        return items;
    }

    public void setItems(Map<String, Item> items) {
        this.items = items;
    }

    public Cart(){
        items = new HashMap<String, Item>();

    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }




    public User getCustomer() {
        return customer;
    }

    public void setCustomer(User customer) {
        this.customer = customer;
    }
}

