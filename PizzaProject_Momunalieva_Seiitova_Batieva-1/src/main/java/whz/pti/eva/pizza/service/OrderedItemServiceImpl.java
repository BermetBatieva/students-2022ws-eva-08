package whz.pti.eva.pizza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import whz.pti.eva.pizza.domain.*;
import whz.pti.eva.security.domain.UserRepository;

import javax.transaction.Transactional;
import java.security.Provider;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderedItemServiceImpl {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private  OrderedItemRepository orderedItemRepository;

    @Autowired
    private OrderedRepository orderedRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SmmpService smmpService;

    @Transactional
    public Long createOrder(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Cart cart = cartRepository.findByCustomer_LoginNameAndStatus(auth.getName(), Status.ACTIVE);
            Ordered order = new Ordered();
            List<OrderedItem> orderedItemList = new ArrayList<>();
            float totalPrice = 0;
            for (Item item : cart.getItems().values().stream().toList()) {
                OrderedItem orderedItem = new OrderedItem();
                orderedItem.setQuantity(item.getQuantity());
                orderedItem.setName(item.getPizza().getName());
                orderedItem.setOrderedItemPrice(item.getItemPrice());
                orderedItem.setPizzaSize(item.getPizzaSize());
                orderedItem.setUserId(cart.getCustomer().getId().toString());
                orderedItem.setPizzaId(item.getPizza().getId());
                orderedItemRepository.save(orderedItem);
                orderedItemList.add(orderedItem);
                totalPrice += orderedItem.getOrderedItemPrice().floatValue();
            }

            if(smmpService.doPayAction(auth.getName(), "transfer"+" "+totalPrice)
                    .getDescription().equals("Nicht genug Geld")){
                return 0l;
            }

            order.setNumberOfItems(orderedItemList.size());
            order.setItems(orderedItemList);
            order.setTotalPrice(totalPrice);
            orderedRepository.save(order);
            order.setUserId(userRepository.getUserByLoginName(auth.getName()).getId().toString());
            orderedRepository.save(order);
            cart.setStatus(Status.DELETED);
            cartRepository.save(cart);

          return order.getId();
    }





}
