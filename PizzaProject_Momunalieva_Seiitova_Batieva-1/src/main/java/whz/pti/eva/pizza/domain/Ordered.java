package whz.pti.eva.pizza.domain;

import org.hibernate.annotations.Proxy;
import whz.pti.eva.common.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "ordered")
public class Ordered extends BaseEntity<Long> {


    private int numberOfItems;

    private String userId;

    @OneToMany(fetch = FetchType.EAGER)
    private List<OrderedItem> items;

    private float totalPrice;


    public Ordered(int numberOfItems, String userid, List<OrderedItem> items) {
        this.numberOfItems = numberOfItems;
        this.userId = userid;
        this.items = items;
    }

    public Ordered() {

    }

    public void setNumberOfItems(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public void setUserId(String userid) {
        this.userId = userid;
    }

    public void setItems(List<OrderedItem> items) {
        this.items = items;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

    public String getUserId() {
        return userId;
    }

    public List<OrderedItem> getItems() {
        return items;
    }

    public float getTotalPrice() {
        return totalPrice;
    }
}

