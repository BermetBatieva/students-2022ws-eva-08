package whz.pti.eva.pizza.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import whz.pti.eva.pizza.service.OrderedItemServiceImpl;
import whz.pti.eva.pizza.service.OrderedServiceImpl;

import java.security.Principal;

@Controller
@RequestMapping("/ordered")
public class OrderedController {

    @Autowired
    private OrderedItemServiceImpl orderedItemService;

    @Autowired
    private OrderedServiceImpl orderedService;


    @RequestMapping(value = "/get-all-details",method = {RequestMethod.POST, RequestMethod.GET})
    public String getAllOrderedItems(Model model, Principal principal){
        if(principal == null)
            return "redirect:/login";
        else {
            if(orderedService.getAllOrdersByCurrentUser().size() == 0){
                model.addAttribute("messageBestellung", "damit Ihre Bestellung angezeigt wird, klicken Sie bitte auf \"Bestätigung\"");
                return "cart";
            }
            else{
                model.addAttribute("orderedItems", orderedService.getLastOrderByCurrentUser());
                if(orderedService.getAllOrdersOfCurrentUser().size() == 0)
                    model.addAttribute("messageAboutEmptyHistory", "Es gibt keine vorherige Bestellungen");
                else
                    model.addAttribute("prevOrders", orderedService.getAllOrdersOfCurrentUser()); //List
            }

            return "ordered";
        }
    }
}
