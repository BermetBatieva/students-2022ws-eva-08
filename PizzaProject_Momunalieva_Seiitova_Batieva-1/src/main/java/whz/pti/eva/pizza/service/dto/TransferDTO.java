package whz.pti.eva.pizza.service.dto;

import java.io.Serializable;

public class TransferDTO implements Serializable{

    String to = "pizzaAccount";
    double amount;

    public TransferDTO() {
    }

    public TransferDTO( double amount) {
        this.to = "pizzaAccount";
        this.amount = amount;
    }

    public String getTo() {
        return to;
    }

    public double getAmount() {
        return amount;
    }
}

