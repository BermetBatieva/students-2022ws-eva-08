package whz.pti.eva.pizza.domain;

import org.hibernate.annotations.Proxy;
import whz.pti.eva.common.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ordered_item")
public class OrderedItem extends BaseEntity<Long> {

    @Column(name = "pizza_id")
    private long pizzaId;

    private String name;

    private int quantity;

    @Enumerated(EnumType.STRING)
    @Column(name = "pizza_size")
    private PizzaSize pizzaSize;

    @Column(name = "ordered_item_price")
    private BigDecimal orderedItemPrice;

    @Column(name = "user_id")
    private String userId;

    public OrderedItem(long pizzaId, String name, int quantity, PizzaSize pizzaSize, BigDecimal orderedItemPrice, String userId) {
        this.pizzaId = pizzaId;
        this.name = name;
        this.quantity = quantity;
        this.pizzaSize = pizzaSize;
        this.orderedItemPrice = orderedItemPrice;
        this.userId = userId;
    }

    public PizzaSize getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSize pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public BigDecimal getOrderedItemPrice() {
        return orderedItemPrice;
    }

    public void setOrderedItemPrice(BigDecimal orderedItemPrice) {
        this.orderedItemPrice = orderedItemPrice;
    }



    public OrderedItem() {

    }

    public long getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(long pizzaId) {
        this.pizzaId = pizzaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
