package whz.pti.eva.pizza.domain;

import whz.pti.eva.common.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "item")
public class Item  extends BaseEntity<Long> {
    //    unique
    private String itemId;

    private int quantity;

    @Enumerated(EnumType.STRING)
    private PizzaSize pizzaSize;

    @ManyToOne
    private Pizza pizza;

    @Column(name = "item_price")
    private BigDecimal itemPrice;

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Item(String itemId, int quantity, PizzaSize pizzaSize, Pizza pizza, BigDecimal itemPrice) {
        this.itemId = itemId;
        this.quantity = quantity;
        this.pizzaSize = pizzaSize;
        this.pizza = pizza;
        this.itemPrice = itemPrice;
    }

    public Item(){

    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public PizzaSize getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(PizzaSize pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        if (!super.equals(o)) return false;
        Item item = (Item) o;
        return quantity == item.quantity && itemId.equals(item.itemId)
                && pizzaSize == item.pizzaSize && pizza.equals(item.pizza)
                && itemPrice.equals(item.itemPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), itemId, quantity, pizzaSize, pizza, itemPrice);
    }
}
