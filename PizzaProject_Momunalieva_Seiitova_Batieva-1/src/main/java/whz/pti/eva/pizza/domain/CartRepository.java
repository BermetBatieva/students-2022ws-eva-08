package whz.pti.eva.pizza.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import whz.pti.eva.security.domain.User;

import java.util.Map;

public interface CartRepository extends JpaRepository<Cart,Long> {


    Cart findByCustomer_LoginNameAndStatus(String login, Status s);

}
