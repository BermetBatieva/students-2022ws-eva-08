package whz.pti.eva.pizza.service;


import whz.pti.eva.pizza.domain.Pizza;

import java.util.List;

public interface PizzaService {

    List<Pizza> findAll();
}
