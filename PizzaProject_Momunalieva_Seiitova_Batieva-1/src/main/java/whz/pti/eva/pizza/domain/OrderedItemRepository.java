package whz.pti.eva.pizza.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderedItemRepository extends JpaRepository<OrderedItem,Long> {
}
