package whz.pti.eva.pizza.service;

import whz.pti.eva.pizza.domain.Item;
import whz.pti.eva.pizza.domain.PizzaSize;


public interface ItemService {

    public Item addItemToCart(Long pizzaId,int quantity,PizzaSize pizzaSize);
}
