package whz.pti.eva.pizza.boundary;

import org.h2.engine.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import whz.pti.eva.pizza.domain.Item;
import whz.pti.eva.pizza.domain.PizzaSize;
import whz.pti.eva.pizza.service.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


@Controller
@RequestMapping("/pizza")
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
@SessionAttributes("listAllItems")
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private ItemServiceImpl itemService;

    @Autowired
    private CartServiceImpl cartService;


    @Autowired
    private OrderedItemServiceImpl orderedItemService;

    @GetMapping("")
    public String getAllPizza(Model model){
        model.addAttribute("listAllPizza",pizzaService.findAll());
        model.addAttribute("getAmountOfItems", itemService.countItemsInCart());
        model.addAttribute("getTotalPrice", itemService.countTotalPrise());
        return "pizza";
    }

    @RequestMapping(value = "/add-to-cart", method = {RequestMethod.POST, RequestMethod.GET})
    public String addItem(long pizzaId,
                          int quantity,PizzaSize pizzaSize,Model model)  {
        model.addAttribute("listAllItems", itemService.addToTemporaryCart(pizzaId,quantity,pizzaSize));
        return "redirect:/pizza";
    }

    @GetMapping("/cart")
    public String getItemsByCart(Model model){
        model.addAttribute("listAllItems", itemService.findAllItemsByCart());
        model.addAttribute("getTotalPrice", itemService.countTotalPrise());
        return "cart";
    }

    @RequestMapping(value = "/confirm", method = {RequestMethod.POST})
    public String addCartToOrder(Principal principal,Model model) {
        if(principal == null) {
            return "redirect:/login";
        }
        else {
            if(itemService.addItemToCartAuth().getItems().size() == 0){
                model.addAttribute("message","Warenkorb ist leer!");
                return "cart";
            }else {
                itemService.addItemToCartAuth();
                orderedItemService.createOrder();
            }
            return "redirect:/pizza/cart";
        }
    }

        @RequestMapping ("/delete/{index}")
        public String deleteItemFromCart(Model model,@PathVariable int index){
        itemService.deleteItem(index);
        model.addAttribute("listAllItems", itemService.findAllItemsByCart());
        return "redirect:/pizza/cart";
    }


}


