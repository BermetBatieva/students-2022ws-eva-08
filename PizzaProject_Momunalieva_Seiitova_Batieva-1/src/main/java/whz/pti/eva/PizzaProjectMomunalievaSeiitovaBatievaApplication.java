package whz.pti.eva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaProjectMomunalievaSeiitovaBatievaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaProjectMomunalievaSeiitovaBatievaApplication.class, args);
	}

}
