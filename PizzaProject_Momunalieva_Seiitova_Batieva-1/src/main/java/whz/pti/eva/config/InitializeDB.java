package whz.pti.eva.config;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import whz.pti.eva.pizza.domain.*;
import whz.pti.eva.security.domain.Role;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class InitializeDB {

    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeDB.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PizzaRepository pizzaRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    DeliveryAddressRepository deliveryAddressRepository;

    @PostConstruct
    public void init()  {

        LOGGER.debug("Db initialized");

        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


        User user = new User();
        user.setLoginName("bnutz");
        user.setEmail("bnutz@whz.de");
        user.setLastName("batieva");
        user.setFistName("bermet");
        user.setStatus(Status.ACTIVE);
        user.setPasswordHash(passwordEncoder.encode("n1"));
        user.setRole(Role.valueOf(Role.CUSTOMER.toString()));
        userRepository.save(user);


        User user2 = new User();
        user2.setStatus(Status.ACTIVE);
        user2.setFistName("nuraika");
        user2.setLastName("seiitova");
        user2.setLoginName("cnutz");
        user2.setEmail("cnutz@whz.de");
        user2.setPasswordHash(passwordEncoder.encode("n2"));
        user2.setRole(Role.valueOf(Role.CUSTOMER.toString()));
        userRepository.save(user2);


        User admin = new User();
        admin.setStatus(Status.ACTIVE);
        admin.setLastName("momunalieva");
        admin.setFistName("ainazik");
        admin.setLoginName("admin");
        admin.setEmail("admin@whz.de");
        admin.setPasswordHash(passwordEncoder.encode("a1"));
        admin.setRole(Role.valueOf(Role.ADMIN.toString()));
        userRepository.save(admin);

        Pizza pizza = new Pizza();
        pizza.setName("Pepperoni");
        pizza.setPriceLarge(new BigDecimal("12"));
        pizza.setPriceMedium(new BigDecimal("10"));
        pizza.setPriceSmall(new BigDecimal("7"));
        pizzaRepository.save(pizza);

        Pizza pizza1 = new Pizza();
        pizza1.setName("Margherita");
        pizza1.setPriceLarge(new BigDecimal("13"));
        pizza1.setPriceMedium(new BigDecimal("11"));
        pizza1.setPriceSmall(new BigDecimal("5"));
        pizzaRepository.save(pizza1);

        Pizza pizza2 = new Pizza();
        pizza2.setName("Buffalo");
        pizza2.setPriceLarge(new BigDecimal("9"));
        pizza2.setPriceMedium(new BigDecimal("8"));
        pizza2.setPriceSmall(new BigDecimal("7"));
        pizzaRepository.save(pizza2);

        List<User> list = new ArrayList<>();
        list.add(user);
        list.add(user2);
        list.add(admin);
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setCustomer(list);
        deliveryAddress.setStreet("Innere schneeberger");
        deliveryAddress.setTown("Zwickau");
        deliveryAddress.setPostalCode("08056");
        deliveryAddress.setHouseNumber("23");
        deliveryAddressRepository.save(deliveryAddress);
        List<DeliveryAddress> list1 = new ArrayList<>();
        list1.add(deliveryAddress);

        user.setDeliveryAddress(list1);
        user2.setDeliveryAddress(list1);
        admin.setDeliveryAddress(list1);

        userRepository.save(user);
        userRepository.save(user2);
        userRepository.save(admin);


    }
}

