package whz.pti.eva.security.service.user;

import org.apache.juli.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import whz.pti.eva.pizza.domain.DeliveryAddress;
import whz.pti.eva.pizza.domain.DeliveryAddressRepository;
import whz.pti.eva.pizza.domain.Status;
import whz.pti.eva.pizza.service.SmmpService;
import whz.pti.eva.pizza.service.dto.PayActionResponseDTO;
import whz.pti.eva.security.domain.Role;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserCreateForm;
import whz.pti.eva.security.domain.UserRepository;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import whz.pti.eva.security.service.dto.UserDTO;

import javax.transaction.Transactional;


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private SmmpService smmpService;

    @Override
    @Transactional
    public User create(UserCreateForm form) {
        PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        User user = new User();
        List<User> list =new ArrayList<>();
        user.setStatus(Status.ACTIVE);
        user.setEmail(form.getEmail());
        user.setLoginName(form.getLoginName());
        user.setPasswordHash(passwordEncoder.encode(form.getPassword()));
        user.setFistName(form.getFirstName());
        user.setLastName(form.getLastName());
        user.setRole(Role.CUSTOMER);
        userRepository.save(user);
        list.add(user);
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        List<DeliveryAddress> list1 = new ArrayList<>();
        deliveryAddress.setTown(form.getTown());
        deliveryAddress.setPostalCode(form.getPostalCode());
        deliveryAddress.setStreet(form.getStreet());
        deliveryAddress.setHouseNumber(form.getHouseNumber());
        deliveryAddressRepository.save(deliveryAddress);
        deliveryAddress.setCustomer(list);
        deliveryAddressRepository.save(deliveryAddress);
        list1.add(deliveryAddress);
        user.setDeliveryAddress(list1);
        userRepository.save(user);
        smmpService.doPayAction(user.getLoginName(),"open");
        return user;
    }

    @Override
    public boolean existsByLoginName(String loginName) {
        return userRepository.existsByLoginName(loginName);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public Optional<User> findByLoginName(String login) {
        return userRepository.findByLoginName(login);
    }

    @Override
    public User getInfoCurrentUser(Principal principal){
        return userRepository.getUserByLoginName(principal.getName());
    }

    @Override
    public List<UserDTO> getAllUsers() {
        List<User> targetListOrigin = userRepository.findByStatusAndRole(Status.ACTIVE, Role.CUSTOMER);
        List<UserDTO> targetList= new ArrayList<>();
        for (User source: targetListOrigin ) {
            UserDTO target= new UserDTO();
            BeanUtils.copyProperties(source , target);
            targetList.add(target);
        }
        return targetList;
    }

    @Override
    public User deleteUser(String loginName) {
        User user = userRepository.getUserByLoginName(loginName);
        user.setStatus(Status.DELETED);
        return userRepository.save(user);

    }


}
