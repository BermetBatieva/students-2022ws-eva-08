package whz.pti.eva.security.domain;


import javax.validation.constraints.NotEmpty;

public class UserCreateForm {

    @NotEmpty
    private String loginName = "";

    @NotEmpty
    private String email = "";

    @NotEmpty
    private String password = "";

    @NotEmpty
    private String street = "";
    @NotEmpty
    private String houseNumber = "";
    @NotEmpty
    private String town = "";
    @NotEmpty
    private String postalCode = "";

    @NotEmpty
    private String firstName = "";

    @NotEmpty
    private String lastName = "";



    public UserCreateForm(){

    }


    @NotEmpty()
    private String passwordRepeated = "";

    public UserCreateForm(String loginName, String email, String password, String street, String houseNumber, String town, String postalCode, String firstName, String lastName, String passwordRepeated) {
        this.loginName = loginName;
        this.email = email;
        this.password = password;
        this.street = street;
        this.houseNumber = houseNumber;
        this.town = town;
        this.postalCode = postalCode;
        this.firstName = firstName;
        this.lastName = lastName;
        this.passwordRepeated = passwordRepeated;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeated() {
        return passwordRepeated;
    }

    public void setPasswordRepeated(String passwordRepeated) {
        this.passwordRepeated = passwordRepeated;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

