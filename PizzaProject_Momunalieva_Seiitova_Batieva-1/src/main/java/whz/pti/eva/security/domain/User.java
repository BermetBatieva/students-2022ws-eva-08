package whz.pti.eva.security.domain;

import org.hibernate.annotations.Proxy;
import whz.pti.eva.common.BaseEntity;
import whz.pti.eva.pizza.domain.DeliveryAddress;
import whz.pti.eva.pizza.domain.Status;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "secuser")
@Proxy(lazy=false)
public class User extends BaseEntity<Long> {

    @Column(name = "login_name", nullable = false, unique = true)
    private String loginName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    private String fistName;

    private String lastName;

    @Enumerated(EnumType.STRING)
    private Status status;

    @ManyToMany
    @JoinTable(
            name = "delivery_address_customer",
            joinColumns = @JoinColumn(name = "customer_id"),
            inverseJoinColumns = @JoinColumn(name = "delivery_address_id"))
    private List<DeliveryAddress> deliveryAddress;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User(String loginName, String email, String passwordHash, Role role, String fistName, String lastName, Status status, List<DeliveryAddress> deliveryAddress) {
        this.loginName = loginName;
        this.email = email;
        this.passwordHash = passwordHash;
        this.role = role;
        this.fistName = fistName;
        this.lastName = lastName;
        this.status = status;
        this.deliveryAddress = deliveryAddress;
    }

    public User() {
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<DeliveryAddress> getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(List<DeliveryAddress> deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }
}

