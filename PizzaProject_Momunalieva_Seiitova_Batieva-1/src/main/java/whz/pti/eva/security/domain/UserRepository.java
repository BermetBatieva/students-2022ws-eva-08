package whz.pti.eva.security.domain;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import whz.pti.eva.pizza.domain.Status;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByLoginName(String loginName);

    boolean existsByEmail(String email);

    Optional<User> findByLoginName(String login);

    User getUserByLoginName(String login);
    User findByEmail(String email);




     List<User> findByStatusAndRole(Status status, Role role);
}

