package whz.pti.eva.security.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import whz.pti.eva.security.domain.UserCreateForm;
import whz.pti.eva.security.service.user.UserService;
import whz.pti.eva.security.service.validator.UserCreateFormValidator;

import javax.validation.Valid;

@Controller
public class RegistrationController {


    @Autowired
    private UserCreateFormValidator userCreateFormValidator;

    @Autowired
    private UserService userService;



    @InitBinder("myform")
    public void initBinder(WebDataBinder binder) {
        binder.addValidators(userCreateFormValidator);
    }


    @GetMapping("/register")
    public String showRegistrationForm(){
        return "user_create";
    }


    @RequestMapping(value = "/users/create", method = {RequestMethod.POST,RequestMethod.GET})
    public String handleUserCreateForm(@Valid @ModelAttribute("myform") UserCreateForm form,
                                       BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error",
                    bindingResult.getGlobalError().getDefaultMessage());
            return "user_create";
        }
        userService.create(form);
        return "redirect:/login";
    }

}

