package whz.pti.eva.security.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import whz.pti.eva.pizza.domain.DeliveryAddress;
import whz.pti.eva.pizza.domain.DeliveryAddressRepository;
import whz.pti.eva.security.domain.UserRepository;
import whz.pti.eva.security.service.user.UserService;

import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private UserRepository userRepository;

//
    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/users/managed", method = {RequestMethod.GET})
    public String getUserManagedPage(Model model, Principal principal) {
        model.addAttribute("admin",principal);
        model.addAttribute("users", userService.getAllUsers());
        return "all_users";
    }

    @RequestMapping("/users/delete/{loginName}")
    public String deleteByUserId(Model model,@PathVariable String loginName){
        userService.deleteUser(loginName);
        model.addAttribute("users", userService.getAllUsers());
        return "all_users";
    }

    @GetMapping("/user/info")
    public String getInfoAboutCurrentUser(Model model,Principal principal){
        List<DeliveryAddress> deliveryAddresses =
                deliveryAddressRepository.findDeliveryAddressByCustomerId(userRepository.getUserByLoginName(principal.getName()).getId());
        model.addAttribute("address",deliveryAddresses);
        model.addAttribute("user",userService.getInfoCurrentUser(principal));
        return "profile";
    }



}
