package whz.pti.eva.security.service.user;



import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserCreateForm;
import whz.pti.eva.security.service.dto.UserDTO;

import java.security.Principal;
import java.util.Collection;
import java.util.Optional;


public interface UserService {

    User create(UserCreateForm form);

    boolean existsByLoginName(String loginName);

    boolean existsByEmail(String email);


    Optional<User> findByLoginName(String login);

    Collection<UserDTO> getAllUsers();

    User deleteUser(String loginName);

    User getInfoCurrentUser(Principal principal);


    }

