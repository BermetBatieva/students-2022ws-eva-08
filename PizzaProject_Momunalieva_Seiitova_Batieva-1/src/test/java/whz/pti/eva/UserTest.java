package whz.pti.eva;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import whz.pti.eva.pizza.domain.DeliveryAddress;
import whz.pti.eva.pizza.domain.DeliveryAddressRepository;
import whz.pti.eva.pizza.domain.Status;
import whz.pti.eva.security.domain.Role;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = PizzaProjectMomunalievaSeiitovaBatievaApplication.class)
public class UserTest {

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private UserRepository userRepository;

    PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


    @Test
    public void test1(){
        List<DeliveryAddress> deliveryAddress = new ArrayList<>();
        DeliveryAddress address = new DeliveryAddress();
        address.setHouseNumber("23");
        address.setPostalCode("08056");
        address.setStreet("Innere Schneeberger");
        address.setTown("Zwickau");
        deliveryAddressRepository.save(address);
        deliveryAddress.add(address);
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setStatus(Status.ACTIVE);
        user.setRole(Role.CUSTOMER);
        user.setLoginName("nura");
        user.setEmail("nura@gmail.com");
        user.setPasswordHash(passwordEncoder.encode("123"));
        user.setDeliveryAddress(deliveryAddress);
        user.setFistName("nura");
        user.setLastName("seiitova");
        userRepository.save(user);
        users.add(user);
        address.setCustomer(users);
        deliveryAddressRepository.save(address);

        Optional<User> user1 = userRepository.findByLoginName(user.getLoginName());

        assertThat(user1.get().getEmail()).isEqualTo("nura@gmail.com");
    }


    @Test
    public void test2(){
        List<DeliveryAddress> deliveryAddress = new ArrayList<>();
        DeliveryAddress address = new DeliveryAddress();
        address.setHouseNumber("23");
        address.setPostalCode("08056");
        address.setStreet("Aüßere Schneeberger");
        address.setTown("Zwickau");
        deliveryAddressRepository.save(address);
        deliveryAddress.add(address);
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setStatus(Status.ACTIVE);
        user.setRole(Role.CUSTOMER);
        user.setLoginName("nuraika");
        user.setEmail("nuraika@gmail.com");
        user.setPasswordHash(passwordEncoder.encode("nuraika"));
        user.setDeliveryAddress(deliveryAddress);
        user.setFistName("nuraika");
        user.setLastName("sei");
        userRepository.save(user);
        users.add(user);
        address.setCustomer(users);
        deliveryAddressRepository.save(address);

       List<User>  list = userRepository.findByStatusAndRole(user.getStatus(), user.getRole());

        assertThat(list.size()).isEqualTo(4); //in db exist 3 users
        assertThat(list.get(3).getRole()).isEqualTo(Role.CUSTOMER);
    }
}
