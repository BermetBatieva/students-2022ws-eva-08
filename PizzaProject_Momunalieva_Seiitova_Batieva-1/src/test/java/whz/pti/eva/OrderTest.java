package whz.pti.eva;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import whz.pti.eva.pizza.domain.*;
import whz.pti.eva.security.domain.Role;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserRepository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = PizzaProjectMomunalievaSeiitovaBatievaApplication.class)
public class OrderTest {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderedRepository orderedRepository;
    PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    @Test
    public void test1(){
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setStatus(Status.ACTIVE);
        user.setRole(Role.CUSTOMER);
        user.setLoginName("zayn");
        user.setEmail("zayn@gmail.com");
        user.setPasswordHash(passwordEncoder.encode("zayn"));
        user.setFistName("zayn");
        user.setLastName("zayn");
        userRepository.save(user);
        users.add(user);

        List<Ordered> orders = orderedRepository.
                findByUserId((userRepository.getUserByLoginName(user.getLoginName()).getId().toString()));

        assertThat(orders.size()).isEqualTo(0);
    }
}
