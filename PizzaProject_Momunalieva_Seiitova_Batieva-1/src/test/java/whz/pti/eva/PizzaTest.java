package whz.pti.eva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import whz.pti.eva.pizza.domain.Pizza;
import whz.pti.eva.pizza.domain.PizzaRepository;

import org.junit.jupiter.api.Test;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;



@SpringBootTest(classes = PizzaProjectMomunalievaSeiitovaBatievaApplication.class)
public class PizzaTest {

    @Autowired
    private PizzaRepository pizzaRepository;

    @Test
    public void test1() {
        Pizza pizza = new Pizza();
        pizza.setPriceSmall(new BigDecimal("9"));
        pizza.setName("Capricciosa");
        pizza.setPriceMedium(new BigDecimal("10"));
        pizza.setPriceLarge(new BigDecimal("12"));
        pizzaRepository.save(pizza);
        assertThat(pizza.getName()).isEqualTo("Capricciosa");
    }

    @Test
    public void test2(){
        List<Pizza> pizzas = new ArrayList<>();
        Pizza pizza = new Pizza();
        pizza.setPriceSmall(new BigDecimal("9"));
        pizza.setName("Margherita");
        pizza.setPriceMedium(new BigDecimal("10"));
        pizza.setPriceLarge(new BigDecimal("12"));
        pizzaRepository.save(pizza);
        pizzas.add(pizza);

        assertThat(pizzas.size()).isEqualTo(1);
    }




}
