package whz.pti.eva;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import whz.pti.eva.pizza.domain.*;
import whz.pti.eva.pizza.service.ItemService;
import whz.pti.eva.pizza.service.ItemServiceImpl;
import whz.pti.eva.security.domain.Role;
import whz.pti.eva.security.domain.User;
import whz.pti.eva.security.domain.UserRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = PizzaProjectMomunalievaSeiitovaBatievaApplication.class)
public class CartTest {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private UserRepository userRepository;


    PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();


    @Test
    public void test1() {
        List<DeliveryAddress> deliveryAddress = new ArrayList<>();
        DeliveryAddress address = new DeliveryAddress();
        address.setHouseNumber("12");
        address.setPostalCode("0856");
        address.setStreet("Innere schneeberger");
        address.setTown("Zwickau");
        deliveryAddressRepository.save(address);
        deliveryAddress.add(address);
        List<User> users = new ArrayList<>();
        User user = new User();
        user.setStatus(Status.ACTIVE);
        user.setRole(Role.CUSTOMER);
        user.setLoginName("bema");
        user.setEmail("bema@gmail.com");
        user.setPasswordHash(passwordEncoder.encode("n1"));
        user.setDeliveryAddress(deliveryAddress);
        user.setFistName("bermet");
        user.setLastName("batieva");
        userRepository.save(user);
        users.add(user);
        address.setCustomer(users);
        deliveryAddressRepository.save(address);

        Cart customerCart = new Cart();
        customerCart.setStatus(Status.ACTIVE);
        customerCart.setCustomer(user);
        cartRepository.save(customerCart);
        Cart cart = cartRepository.findByCustomer_LoginNameAndStatus(user.getLoginName(), Status.ACTIVE);
        assertThat(cart.getCustomer().getLoginName()).isEqualTo("bema");
    }

}
